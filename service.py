# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement

from datetime import date, time, timedelta, datetime
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pyson import Eval, If, In, Get, Not
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.modules.company import CompanyReport
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.report import Report

__all__ = ['Service', 'ServiceLine', 'SpaServiceReport',
    'SpaServiceSale', 'ScheduleByProfessionalReport',
    'ScheduleByProfessional', 'ScheduleByProfessionalStart',
    'ScheduleByDay', 'ScheduleByDayStart', 'ScheduleByDayReport']

STATES = {
    'readonly': (Eval('state') != 'draft'),
}

STATES_DONE = {
    'readonly': (Eval('state') != 'draft'),
    'required': (Eval('state') == 'done'),
}

SATISFACTION = [
    ('', ''),
    ('5', 'Excellent'),
    ('4', 'Good'),
    ('3', 'Regular'),
    ('2', 'Bad'),
    ('1', 'Worse'),
]


class Service(Workflow, ModelSQL, ModelView):
    'Spa Service'
    __name__ = 'spa.service'
    _rec_name = 'number'
    number = fields.Char('Number', readonly=True)
    party = fields.Many2One('party.party', 'Party', required=True,
            states=STATES, select=True)
    reference = fields.Char('Reference', states=STATES, select=True)
    phone = fields.Char('Phone', states=STATES, select=True,
            depends=['party'], required=True)
    description = fields.Char('Description', required=True, states=STATES)
    schedule_date = fields.Date('Schedule Date', states=STATES, required=True)
    company = fields.Many2One('company.company', 'Company', required=True,
            states=STATES, domain=[ ('id', If(In('company',
            Eval('context', {})), '=', '!='), Get(Eval('context', {}),
            'company', 0)), ])
    satisfaction = fields.Selection(SATISFACTION, 'Satisfaction',
            states=STATES_DONE)
    payment_term = fields.Many2One('account.invoice.payment_term',
        'Payment Term', states=STATES, required=True)
    salesman = fields.Many2One('company.employee',
        'Salesman', states={
            'required': Eval('state') == 'done',
            'readonly': Eval('state').in_(['done', 'cancelled', 'no_submitted']),
        })
    notes = fields.Text('Notes', states=STATES_DONE)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
            ('cancelled', 'Cancelled'),
            ('submitted', 'Submitted'),
            ('no_submitted', 'No Submitted'),
            ('done', 'Done'),
        ], 'State', readonly=True, required=True)
    state_string = state.translated('state')
    media = fields.Selection([
            ('', ''),
            ('phone', 'Phone'),
            ('fax', 'Fax'),
            ('mail', 'Mail'),
            ('videochat', 'Videochat'),
            ('whatsap', 'Whatsap'),
            ('direct', 'Direct'),
            ('web', 'Web'),
            ('other', 'Other'),
        ], 'Media', states=STATES, required=True)
    lines = fields.One2Many('spa.service.line', 'service', 'Lines',
        states={
            'required': Eval('state') == 'confirmed',
            'readonly': Not(In(Eval('state'), ['draft', 'confirmed'])),
        }, depends=['state', 'party'], context={
            'party': Eval('party'),
            'schedule_date': Eval('schedule_date'),
        })
    sales = fields.Many2Many('spa.service-sale.sale', 'service',
         'sale', 'Sales', readonly=True)

    @classmethod
    def __setup__(cls):
        super(Service, cls).__setup__()
        cls._order.insert(0, ('schedule_date', 'DESC'))
        cls._error_messages.update({
            'missing_sequence_spa_service': ('The sequence for '
                'spa service is missing!'),
        })
        cls._transitions |= set((
                ('draft', 'confirmed'),
                ('draft', 'cancelled'),
                ('cancelled', 'draft'),
                ('confirmed', 'draft'),
                ('confirmed', 'cancelled'),
                ('confirmed', 'submitted'),
                ('confirmed', 'no_submitted'),
                ('submitted', 'confirmed'),
                ('submitted', 'draft'),
                ('submitted', 'done'),
                ('done', 'draft'),
                ))
        cls._buttons.update({
                'draft': {
                    'invisible': Eval('state') == 'draft',
                    },
                'cancel': {
                    'invisible': ~Eval('state').in_(['draft', 'confirmed']),
                    },
                'confirm': {
                    'invisible': Eval('state') != 'draft',
                    },
                'submitted': {
                    'invisible': Eval('state') != 'confirmed',
                    },
                'no_submitted': {
                    'invisible': Eval('state') != 'confirmed',
                    },
                'done': {
                    'invisible': Eval('state') != 'submitted',
                    },
                })

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('party',) + tuple(clause[1:]),
            ('number',) + tuple(clause[1:]),
        ]

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_payment_term():
        PaymentTerm = Pool().get('account.invoice.payment_term')
        payment_terms = PaymentTerm.search([])
        if payment_terms:
            return payment_terms[0].id

    @staticmethod
    def default_schedule_date():
        return date.today()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, records):
        for service in records:
            service.set_number()

    @classmethod
    @ModelView.button
    @Workflow.transition('submitted')
    def submitted(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('no_submitted')
    def no_submitted(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        for record in records:
            cls.create_sale(record)

    @fields.depends('party', 'phone')
    def on_change_party(self):
        if self.party:
            if self.party.mobile:
                self.phone = self.party.mobile
            elif self.party.phone:
                self.phone = self.party.phone

    def set_number(self):
        '''
        Set sequence number
        '''
        pool = Pool()
        Config = pool.get('spa.configuration')
        Sequence = pool.get('ir.sequence')
        config = Config.get_configuration()
        if not config.spa_service_sequence:
            self.raise_user_error('missing_sequence_spa_service')

        seq_id = config.spa_service_sequence.id
        self.write([self], {'number': Sequence.get_id(seq_id)})

    @classmethod
    def get_sale(cls, service, party, date_):
        pool = Pool()
        Sale = pool.get('sale.sale')
        Party = pool.get('party.party')
        if service.description:
            description = service.description + ' No ' + str(service.number)
        return Sale(
            company=service.company.id,
            payment_term=service.payment_term.id,
            party=party.id,
            sale_date=date_,
            description=description,
            salesman=service.salesman,
            state='draft',
            origin=service,
            invoice_address=Party.address_get(party, type='invoice'),
            shipment_address=Party.address_get(party, type='delivery'),
        )

    @classmethod
    def create_sale(cls, service):
        pool = Pool()
        SaleLine = pool.get('sale.line')

        if not service.payment_term:
            cls.raise_user_error('payterm_missing')

        sale = cls.get_sale(service, service.party, service.schedule_date)
        sale.save()
        for line in service.lines:
            unit_price = line.product.list_price
            new_line = cls.create_sale_line(
                sale, 1, line.product, unit_price,
            )
            SaleLine.create([new_line])

        cls.write([service], {'sales': [('add', [sale.id])]})


    @classmethod
    def create_sale_line(cls, sale, quantity, product, unit_price):
        taxes = []
        if product.customer_taxes_used:
            taxes = product.customer_taxes_used
        new_line = {
            'sale': sale.id,
            'type': 'line',
            'unit': product.template.default_uom.id,
            'quantity': quantity,
            'unit_price': unit_price,
            'product': product.id,
            'description': product.rec_name,
        }
        if taxes:
            taxes_ids = [t.id for t in taxes]
            new_line.update({'taxes': [('add', taxes_ids)]})
        return new_line


class ServiceLine(ModelSQL, ModelView):
    'Service Line'
    __name__ = 'spa.service.line'
    service = fields.Many2One('spa.service', 'Service',
            ondelete='CASCADE', select=True, required=True)
    start_date = fields.DateTime('Start Date', required=True)
    end_date = fields.DateTime('End Date', required=True)
    product = fields.Many2One('product.product', 'Product',
        select=True, required=True, domain=[
            ('type', 'in', ['goods', 'service']),
            ('salable', '=', True),
        ])
    description = fields.Text('Description', select=True, required=True)
    professional = fields.Many2One('spa.professional',
        'Professional', select=True)
    second_professional = fields.Many2One('spa.professional',
        'Second Professional', select=True)
    uom = fields.Many2One('product.uom', 'UOM', readonly=True)
    customer = fields.Many2One('party.party', 'Customer',
            select=True)
    location = fields.Many2One('company.location', 'Location')
    unit_digits = fields.Function(fields.Integer('Unit Digits'),
        'get_unit_digits')
    notes = fields.Text('Notes')

    @classmethod
    def __setup__(cls):
        super(ServiceLine, cls).__setup__()
        cls._order.insert(0, ('start_date', 'ASC'))
        cls._error_messages.update({
            'invalid_start_end_date': ('End date must be greater than'
                ' start date.'),
            'overlap_dates_for_professional': ('The date for this professional'
                ' overlap another date!'),
        })

    @staticmethod
    def default_customer():
        party_id = Transaction().context.get('party')
        if party_id:
            return party_id

    @staticmethod
    def default_start_date():
        date_ = Transaction().context.get('schedule_date')
        if date_:
            return datetime.combine(date_, time(13, 0, 0))

    @fields.depends('start_date', 'end_date')
    def on_change_start_date(self):
        if self.start_date:
            self.end_date = self.start_date + timedelta(hours=1)

    @fields.depends('product', 'description')
    def on_change_product(self):
        if self.product:
            res = self.product.name
            if self.product.description:
                 res += ' | ' + self.product.description
            self.description = res

    @classmethod
    def validate(cls, lines):
        super(ServiceLine, cls).validate(lines)
        for line in lines:
            line.check_dates()
            line.check_resources()

    def check_dates(self):
        if self.start_date >= self.end_date:
            self.raise_user_error('invalid_start_end_date')

    def check_resources(self):
        if self.professional:
            lines = self.search([
                    ('id', '!=', self.id),
                    ('professional', '=', self.professional.id),
                    ('service.state', 'in', ['draft', 'submitted', 'confirmed', 'done']),
                    ['OR', [
                        ('start_date', '>=', self.start_date),
                        ('start_date', '<=', self.end_date),
                    ], [
                        ('end_date', '>=', self.start_date),
                        ('end_date', '<=', self.end_date),
                    ], [
                        ('start_date', '<=', self.start_date),
                        ('end_date', '>=', self.end_date),
                    ]
            ]])
            if lines:
                self.raise_user_error('overlap_dates_for_professional')


class SpaServiceSale(ModelSQL):
    'Spa Service - Sale'
    __name__ = 'spa.service-sale.sale'
    _table = 'service_sales_rel'
    service = fields.Many2One('spa.service', 'Service',
            ondelete='CASCADE', select=True, required=True)
    sale = fields.Many2One('sale.sale', 'Sale',
            ondelete='RESTRICT', select=True, required=True)


class SpaServiceReport(CompanyReport):
    __name__ = 'spa.service'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(SpaServiceReport, cls).get_context(records, data)
        pool = Pool()
        Company = pool.get('company.company')
        for rec in records:
            new_lines = []
            for line in rec.lines:
                line.start_date
                st_date = Company.convert_timezone(line.start_date)
                en_date = Company.convert_timezone(line.end_date)
                setattr(line, 'start_date', st_date)
                setattr(line, 'end_date', en_date)
                new_lines.append(line)
            rec.lines = new_lines
        return report_context


class ScheduleByProfessionalStart(ModelView):
    'Schedule By Professional Start'
    __name__ = 'spa.print_schedule_by_professional.start'
    start_date = fields.Date('Start Date', required=True)
    professional = fields.Many2One('spa.professional',
        'Professional', select=True, required=True)
    company = fields.Many2One('company.company', 'Company', required=True)

    @staticmethod
    def default_start_date():
        today = date.today()
        return today - timedelta(today.weekday())

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False


class ScheduleByProfessional(Wizard):
    'Schedule By Professional'
    __name__ = 'spa.print_schedule_by_professional'
    start = StateView('spa.print_schedule_by_professional.start',
            'spa.print_schedule_by_professional_start_view_form', [
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Print', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateReport('spa.schedule_by_professional.report')

    def do_print_(self, action):
        data = {
            'start_date': self.start.start_date,
            'professional': self.start.professional.id,
            'company': self.start.company.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ScheduleByProfessionalReport(Report):
    'Schedule By Professional Report'
    __name__ = 'spa.schedule_by_professional.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ScheduleByProfessionalReport, cls).get_context(records, data)
        MAX_DAYS = 14
        pool = Pool()
        Company = pool.get('company.company')
        ServiceLine = pool.get('spa.service.line')
        Professional = pool.get('spa.professional')
        alldays = {}
        alldays_convert = {}
        for nd in range(MAX_DAYS):
            day_n = 'day' + str((nd + 1))
            tdate = data['start_date'] + timedelta(nd)
            data[day_n] = tdate
            data['total_' + day_n] = 0
            data[('event_' + day_n)] = 0
            alldays[day_n] = ''
            alldays_convert[tdate] = day_n

        time_ = time(0, 0)
        date_init = datetime.combine(data['start_date'], time_)
        date_init = Company.convert_timezone(date_init)

        date_limit = datetime.combine(data['start_date'] + timedelta(MAX_DAYS), time_)
        date_limit = Company.convert_timezone(date_limit)

        services = ServiceLine.search([
            ('start_date', '>=', date_init),
            ('start_date', '<=', date_limit),
            ('professional', '=', data['professional']),
            ('service.state', 'in', ['draft', 'submitted', 'confirmed', 'done']),
        ])

        lines = {}
        for i in range(24):
            lines[i] = {'hour': time(i, 0)}
            lines[i].update(alldays.copy())

        for service in services:
            tz_start_date = Company.convert_timezone(service.start_date)
            tz_end_date = Company.convert_timezone(service.end_date)
            day_n = alldays_convert[tz_start_date.date()]
            description = []
            if service.customer or service.location:
                if service.customer:
                    description.append(service.customer.name)
                if service.location:
                    description.append(service.location.name)
                description = ' | '.join(description)
            else:
                description = 'X'

            lines[tz_start_date.time().hour][day_n] = description
            delta = (tz_end_date - tz_start_date)
            data['total_' + day_n] += delta.total_seconds() / 3600
            data['event_' + day_n] += 1

        report_context['records'] = lines.values()
        report_context['professional'] = Professional(data['professional']).professional
        report_context['date'] = data['start_date']
        report_context['company'] = Company(data['company']).rec_name
        return report_context


class ScheduleByDayStart(ModelView):
    'Schedule By Day Start'
    __name__ = 'spa.print_schedule_by_day.start'
    date = fields.Date('Start Date', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)

    @staticmethod
    def default_date():
        return date.today()

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False


class ScheduleByDay(Wizard):
    'Schedule By Day'
    __name__ = 'spa.print_schedule_by_day'
    start = StateView('spa.print_schedule_by_day.start',
            'spa.print_schedule_by_day_start_view_form', [
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Print', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateReport('spa.schedule_by_day.report')

    def do_print_(self, action):
        data = {
            'date': self.start.date,
            'company': self.start.company.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ScheduleByDayReport(Report):
    'Schedule By Day Report'
    __name__ = 'spa.schedule_by_day.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ScheduleByDayReport, cls).get_context(records, data)
        pool = Pool()
        Company = pool.get('company.company')
        ServiceLine = pool.get('spa.service.line')
        date_start = Company.convert_timezone(
            datetime.combine(data['date'], time(0,0)), True)
        date_end = Company.convert_timezone(
            datetime.combine(data['date'], time(23,59)), True)
        records = ServiceLine.search([
            ('start_date', '>=', date_start),
            ('start_date', '<=', date_end),
            ], order=[('start_date', 'ASC')])
        for record in records:
            record.start_date = Company.convert_timezone(record.start_date)
            record.end_date = Company.convert_timezone(record.end_date)
        report_context['records'] = records
        report_context['date'] = data['date']
        report_context['company'] = Company(data['company']).rec_name
        return report_context
