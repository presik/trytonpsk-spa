# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import If, Eval
from trytond.transaction import Transaction

__all__ = ['Configuration']


class Configuration(ModelSQL, ModelView):
    'Spa Configuration'
    __name__ = 'spa.configuration'
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', 0)),
            ], select=True)
    spa_service_sequence = fields.Many2One('ir.sequence',
        'Spa Sequence', required=True, domain=[
                ('code', '=', 'spa.service')])

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @classmethod
    def get_configuration(cls):
        company_id = Transaction().context.get('company')
        if not company_id:
            return
        config, = cls.search([
            ('company', '=', company_id)
        ])
        return config
