# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

__all__ = ['SpaProfessional']


class SpaProfessional(ModelSQL, ModelView):
    "Spa Professional"
    __name__ = "spa.professional"
    _rec_name = 'professional'
    professional = fields.Many2One('party.party', 'Professional',
        select=True, required=True)
    active = fields.Boolean('Active', select=True)

    @classmethod
    def __setup__(cls):
        super(SpaProfessional, cls).__setup__()
        cls._order.insert(0, ('professional', 'ASC'))

    @staticmethod
    def default_active():
        return True

    def get_rec_name(self, name):
        rec_name = self.professional.name
        return (rec_name)
