# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from .professional import SpaProfessional
from .configuration import Configuration
from .service import (Service, ServiceLine, SpaServiceReport,
    ScheduleByProfessionalReport, ScheduleByDayReport, ScheduleByDay,
    ScheduleByProfessional, SpaServiceSale, ScheduleByProfessionalStart,
    ScheduleByDayStart)
from .sale import Sale

def register():
    Pool.register(
        Sale,
        Configuration,
        SpaProfessional,
        Service,
        ServiceLine,
        SpaServiceSale,
        ScheduleByProfessionalStart,
        ScheduleByDayStart,
        module='spa', type_='model')
    Pool.register(
        SpaServiceReport,
        ScheduleByProfessionalReport,
        ScheduleByDayReport,
        module='spa', type_='report')
    Pool.register(
        ScheduleByProfessional,
        ScheduleByDay,
        module='spa', type_='wizard')
