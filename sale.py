#This file is part of Presik.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool

__all__ = ['Sale']


class Sale:
    __metaclass__ = PoolMeta
    __name__ = 'sale.sale'

    @property
    def origin_name(self):
        pool = Pool()
        SpaService = pool.get('spa.service')
        name = super(Sale, self).origin_name
        if isinstance(self.origin, SpaService):
            name = self.origin.service.rec_name
        return name

    @classmethod
    def _get_origin(cls):
        origins = super(Sale, cls)._get_origin()
        origins.append('spa.service')
        return origins
